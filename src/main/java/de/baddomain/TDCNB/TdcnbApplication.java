package de.baddomain.TDCNB;

import de.baddomain.TDCNB.util.settingsManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import de.baddomain.TDCNB.apiCalls.*;

import java.io.IOException;

@SpringBootApplication
public class TdcnbApplication {

	public static void main(String[] args) {
		settingsManager sm = new settingsManager();
		System.out.println("888888888888 88888888ba,     ,ad8888ba,  888b      88 88888888ba   \n" +
				           "     88      88      `\"8b   d8\"'    `\"8b 8888b     88 88      \"8b  \n" +
						   "     88      88        `8b d8'           88 `8b    88 88      ,8P  \n" +
						   "     88      88         88 88            88  `8b   88 88aaaaaa8P'  \n" +
						   "     88      88         88 88            88   `8b  88 88\"\"\"\"\"\"8b,  \n" +
						   "     88      88         8P Y8,           88    `8b 88 88      `8b  \n" +
						   "     88      88      .a8P   Y8a.    .a8P 88     `8888 88      a8P  \n" +
						   "     88      88888888Y\"'     `\"Y8888Y\"'  88      `888 88888888P\"   \n Twitch-Discord-Notify-Bot");
		//try to read settings, when fail create file---
		try {
			sm.readSettings();
			SpringApplication.run(TdcnbApplication.class, args); //start spring application for incoming calls
			requestWebhook.subscribe(sm.rp.getProperty("streamer_id"), sm.rp.getProperty("user_id"), sm.rp.getProperty("user_key"), sm.rp.getProperty("callback_url"));//subscribe to twitch webhook
		} catch (IOException e) {
			e.printStackTrace();
			sm.setup();
			System.out.println("Please edit NotifyBot.properties and restart the server to finalize the setup!");
		}
		//-----------------------------------------------
	}

}
