package de.baddomain.TDCNB.apiCalls;

import com.fasterxml.jackson.databind.*;
import de.baddomain.TDCNB.util.settingsManager;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;

public class requestWebhook {

    public static void subscribe(String streamerID, String clientID, String userSecret, String callbackUrl){
        settingsManager sm = new settingsManager();
        try {
            String url = "https://api.twitch.tv/helix/eventsub/subscriptions"; //twitch api url
            String json = "{" +
                    "    \"type\": \"stream.online\"," +
                    "    \"version\": \"1\"," +
                    "    \"condition\": {" +
                    "        \"broadcaster_user_id\": \""+streamerID+"\"" +
                    "    }," +
                    "    \"transport\": {" +
                    "        \"method\": \"webhook\"," +
                    "        \"callback\": \""+callbackUrl+"\"," +
                    "        \"secret\": \" secret123hjiukohij4567\"" +
                    "    }" +
                    "}";   //
            System.out.println(json);

            URL obj = new URL(url); //create url obj
            HttpURLConnection con = (HttpURLConnection) obj.openConnection(); //establish connection

            //set parameters for web request----------------------------------------------------
            con.setRequestProperty("Client-ID", clientID);
            System.out.println(twitchDataFetcher.getToken(clientID, userSecret));
            con.setRequestProperty("Authorization", "Bearer "+ twitchDataFetcher.getToken(clientID, userSecret));
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            //----------------------------------------------------------------------------------

            //post json to api------------------------------------------------------------------
            OutputStream os = con.getOutputStream();
            os.write(json.getBytes(StandardCharsets.UTF_8));
            os.close();
            //-----------------------------------------------------------------------------------

            // read the response
            InputStream in = new BufferedInputStream(con.getInputStream());//convert to readable string
            System.out.println(con.getResponseCode());
            String result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");

            //checks if twitch returned empty, which means success
            System.out.println(result);

            //close all connections, etc
            in.close();
            con.disconnect();
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("[Code 409, in most cases means that the requested webhook is already active. Try deactivating it!]");
        }
    }
}
