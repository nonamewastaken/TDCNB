package de.baddomain.TDCNB.util;
import java.io.*;
import java.util.Properties;

public class settingsManager {

    public Properties rp;
    public Properties wp;

    public void readSettings() throws IOException { //has to be run on every startup
        rp = new Properties();
        InputStream i = new FileInputStream("NotifyBot.properties");
        rp.load(i);
    }

    public void setup(){
        wp = new Properties();
        OutputStream os;
        try {
            os = new FileOutputStream("NotifyBot.properties");
            wp.setProperty("bot_token", "Discord token here");
            wp.setProperty("user_id", "insert your client id here");
            wp.setProperty("user_key", "insert your private key here");
            wp.setProperty("streamer_id", "530688138");
            wp.setProperty("callback_url", "url of the webserver here");
            wp.setProperty("dc_channel", "Allgemein");
            wp.setProperty("bot_icon_url", "https://cdn.discordapp.com/avatars/823883952657924096/5c0596f7ead5be8374fb80c51afb70e1.png?size=128");
            try {
                wp.store(os, null); //saves properties to file
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Finished setup!");
    }
}